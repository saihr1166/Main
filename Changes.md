1. The minSDK is 10, the targetSDK is 19
2. Look at FILE.EXISTS the second parameter is now a value or a String expression!
3.                                                                                                                               
4. The automatic low memory warning is switched off. Use instead OnLowMemory:.
5. The Global Value Backdoor, after an interrupt is trapped, is now closed.

	```
	If getting issues in conjunction with functions in older code 
	created for RFO-Basic 1.91 use:
	ON***:
	GLOBALS.ALL
	….
	GLOBALS.NONE
	***.RESUME
	```
6. The RUN command has new possibilities.
7. AndroidManifest.xml has the new permission ACCESS_NETWORK_STATE.
8. This version use absolute paths instead of and relative paths.
9. An internal directory is created at the first start.
10. Add more system constants at FILE.ROOT
11. FILE.EXISTS returns readable and writeable infos, too.
12. FILE.LASTMODIFIED is new.
13. FILE.EXISTS accepts string expressions and values. See description.
14. BUNDLE.GET and DEBUG.DUMP.BUNDLE convert Java Objects to strings.
15. BUNDLE.TYPE modified
16. BigDecimal implemented
17. ROUND() rounding option Legacy int() added
18. BYTE.COPY append switch added
19. SOCKET.SERVER.READ.BYTE is new
20. SOCKET.CLIENT.READ.BYTE is new
21. Scheduler commands implemented
22. Dialog.message, Dialog.select and Select can now finished at a given time interval
23. At launching with a given program path and an Intent Extra with a key named 	
    “_BASIC!” and a String expression “_Editor” the program starts in the Editor mode.
24. BUNDLE.OUT is new.
25. On Runtime Error a Broadcast is send.
26. Editor improvement: Switching automatic word completions or -corrections to off.

Since OliBasicXXI:

**ATTENTION 
All result arguments of the BigDecimal commands are on the left side now.
I’m so sorry for this inconvenience!**
27. EMAIL.SEND extended with CC, BCC, Attachments ...   [Forgot to list here.]
28. AUDIO.INFO is new.   [Forgot to list here.]
29. AUDIO.RECORD.PEAK is new.   [Forgot to list here.]
30. AUDIO.RECORD.START has new options.   [Forgot to list here.]
31. AUDIO.LOAD a http stream can also be loaded.   [Forgot to list here.]
32. AUDIO.PLAY second parameter for output channel(s)
33. AUDIO.VOLUME third parameter for outer volume control
34. DIR, FILE.DIR, FTP.DIR and ZIP.DIR have a time stamp option now.
35. GR.BITMAP.PUT replaced Bundle.PP
36. GR.BITMAP.GET replaced Bundle.GP
37. TEXT.INPUT improvements: Opportunity to switch off the text suggestion, Menu
38. USING$ needs for %d %o %x %X and %t no more Int().
39. LIST.SORT is new. It sorts the contents of the given list, as an option also in language 	
    and country specific order.
40. REPLACE$ has a new modifier using regular expressions.
41. LIST.SPLIT is new. It splits a list into two new lists item by item.
42. LIST.JOIN is new. It joins two lists into a new list item by item.
43. LIST.MATCH is new. Returns a match found for a search argument in a list.
44. Delimiter and number-of-colums enhancements for SQL.NEW, SQL.INSERT and 	
    SQL.UPDATE. It takes a little more effort because legacy reasons, but it is needed 	
    if you want to import different tables automatically.
45. GR.OPEN improvement: Camera view in background, sorry only Android >= 6 today
46. GR.CAMERA.FOCUS is new in graphics mode in conjunction with GR.OPEN.
47. GR.CAMERA.FLASH is new in graphics mode in conjunction with GR.OPEN.
48. GR.CAMERA.ZOOM is new in graphics mode in conjunction with GR.OPEN.
49. GR.CAMERA.GETPARAM is new in graphics mode, sorry only Android >= 5 today
50. GR.CAMERA.SETPARAM is new in graphics mode, sorry only Android >= 6 today
51. GR.CAMERA.DIRECTSHOOT is new in graphics mode in conjunction with GR.OPEN.
52. Other GR.CAMERA. *** SHOOT commands enhanced with more arguments
53. The SELECT list view can be controlled via the layout parameter bundle now.
54. DIR and FILE.DIR also have access to assets now.
55. CONSOLE.ORIENTATION is new. Works like GR.ORIENTATION
56. CONSOLE.LAYOUT is new.
57. CONSOLE.DEFAULT is new.
58. CONSOLE.LINE.TOUCHED and SELECT also return a double touch tap now.

    To save execution time the following GR commands are a little optimized.
59. GR.SCALE supports as a new option a translation in X and Y direction.
60. GR.SCALE.TOUCH is new and the opposite of GR.SCALE.

    Multi-touch with more than two touches are supported. New commands needed.
61. GR.ARRAY.TOUCH is new and returns pairs of touch coordinates as an array.
62. GR.LIST.TOUCH is new and returns pairs of touch coordinates as list(s).
63. GR.LAST.TOUCH is new and returns the last touch index and coordinates.
64. BYTE.OPEN allow by an internal cache access to converted assets files now.
65. BYTE.COPY break switch added, making an abort by an interrupt possible.
66. SHELL is new. Sends a shell command to the system and wait for a result.
67. SENSORS.LIST is fixed and features new optional sensor infos (Android >= 5).
68. KB.SEND.KEYEVENT is new. Sends key events to the environment.
69. Some USB or Bluetooth keyboard function keys are supported in the Editor now.
70. STT.LISTEN has a new bundle argument to control more options.
71. BACKGROUND checks if the display screen is off and if the device is locked now.
72. FTP.OPEN new opportunity to use FTPS, also.
73. GLOBALS.FNIMP is new. It imports a variable from the main code into a function.

Since OliBasicXXII:

74. ARRAY.BINARY.SEARCH is new. Very fast!
75. LIST.BINARY.SEARCH is new. Very fast!
76. GR.CAMERA.TAKEVIDEO is new.
77. More Editor enhancements, sub menu, and options for code pre handling with 	Basic programs
78. BYTE.WRITE.BUFFER improved for more speed.
79. BYTE.READ.BUFFER, BYTE.WRITE.BUFFER, GRABFILE, GRABURL have or 	have more options to select a character set now.
80. INKEY$ returns the UTF-8 character also.
81. BIGD.FROMDOUBLE is new.
82. BIGD.TODOUBLE is new.
83. BIGD.FROMBASE like BIN, HEX, OCT, but an Integer part greater than 15 	digits is possible.
84. BIGD.TOBASE like BIN$, HEX$, OCT$, as an opposite to BIGD.FROMBASE.
	
	The special Floating Point numbers NaN (Not a Number) and Infinity get better support now.
85. IS_NAN is new and detects NaN (Not a Number) Floating Point numbers
86. IS_INFINITE is new and detects Infinity Floating Point numbers
87. WITHIN() is new. Logical function to search in geometry figures.
88. REVERSE$() is new. Also known as MIRROR$() in other Basic dialects.
89. GR.CLS has a new option to delete all used bitmaps now.
90. If it is possible, bitmaps will be overwritten now.
91. CONSOLE and SELECT are able to print text and images with HTML tags now.
92. New selection opportunities for CONSOLE and SELECT.
93. CLAMP() is new.
94. LIST.JOIN added _min and _max
95. GR_COLLISION New option for setting a distance collision border.
96. GR.BEHIND, GR.INFRONT, GR.TOBACK and GR.TOFRONT are new.
97. Improvements in speed and power consumption associated with console output

Since OliBasicXXIII:

98. BT.UTF_8.READ and BT.UTF_8.WRITE are new. Transfer full UTF-8 char. set.
99. UDP.READ and UDP.WRITE are new and support the UDP protocol.
100. HTTP.REQUEST is new and enhances with more request types and options.
101. GR.COLOR supports also Porter-Duff masking now.
102. GR.PAINT.RESET resets Porter-Duff also.
103. PROGRAM.INFO now extended with memory informations.

Since OliBasicXXIV:

104. SENSORS.READ supports sensors which returns more than 3 parameters.
105. In the Editor’s SubMenu item Previous is new. You can choose one of the ten 
        different last loaded Basic files.
106. APP.SAR supports createChooser().



Some fixes
